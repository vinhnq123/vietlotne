<?php include('./include/header.php') ?>
<div class="container rulers py-2 overflow-auto">
    <h1>Giới thiệu về Keno</h1>
    <!-- start row -->
    <div class="row">
        <div class="col-md-4 d-none d-md-block">
            <div class="list-group" id="menu_item">
                <a href="#thong-tin-tong-quan" class="list-group-item list-group-item-action active">Thông tin tổng
                    quan</a>
                <a href="#co-cau-giai-thuong" class="list-group-item list-group-item-action">Cơ cấu giải thưởng</a>
                <a href="#xem-ket-qua" class="list-group-item list-group-item-action">Xem kết quả</a>
                <a href="#nhan-thuong" class="list-group-item list-group-item-action">Nhận thưởng</a>
                <a href="#cach-choi" class="list-group-item list-group-item-action">Cách chơi</a>

            </div>
        </div>
        <div class="col-md-8 content_rule">
            <div id="thong-tin-tong-quan">
                <h3 style="margin-top: 8px !important">Thông tin tổng quan</h3>
                <p>Chỉ với 10.000đ, bạn có thể chọn từ 1 - 10 số trong 80 số với cơ hội sở hữu giải thưởng lên đến 2 tỷ
                    đồng. KENO quay số mở thưởng mỗi 10 phút 1 lần từ 6h - 22h hàng ngày với 92 kỳ quay số/ngày.</p>
            </div>
            <!-- co cau giai thuong -->
            <div id="co-cau-giai-thuong">
                <h3>Cơ cấu giải thưởng</h3>
                <div class="mb-3">
                    <p>Game sẽ có 10 bậc để khách hàng chọn.</p>
                    <p>Với mỗi bậc, giá trị giải thưởng sẽ khác nhau ở mỗi số lượng số trùng khớp với kết quả:</p>
                    <div class="overflow-auto">
                        <img src="./assets/images/products/co-cau-giai-thuong.png" alt="">
                    </div>
                </div>
                <div id="accordion">
                    <div class="card">
                        <div class="card-header pointer px-0 py-2" data-toggle="collapse" data-target="#ve-khong-trung"
                            aria-expanded="true" aria-controls="ve-khong-trung" id="headingOne">
                            <h5 class="mb-0">
                                <p class="fw-500 text-black">
                                    Đối với vé không trúng kết quả nào
                                </p>
                            </h5>
                        </div>

                        <div id="ve-khong-trung" class="collapse show" aria-labelledby="headingOne"
                            data-parent="#accordion">
                            <p> Bạn chơi ở bậc 1 - 7 => bạn không nhận được giải thưởng</p>
                            <p>Bạn chơi ở bậc 8 - 10 => bạn nhận được 10.000 VND</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pointer px-0 py-2 collapsed" data-toggle="collapse"
                            data-target="#ve-trung-1" aria-expanded="true" aria-controls="ve-trung-1" id="title-1kq">
                            <h5 class="mb-0">
                                <p class="fw-500 text-black">
                                    Đối với vé trúng 1 kết quả
                                </p>
                            </h5>
                        </div>

                        <div id="ve-trung-1" class="collapse" aria-labelledby="title-1kq" data-parent="#accordion">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste suscipit, exercitationem
                                rerum distinctio iusto voluptate rem ipsam provident pariatur. Amet.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pointer px-0 py-2 collapsed" data-toggle="collapse"
                            data-target="#ve-trung-2" aria-expanded="true" aria-controls="ve-trung-2" id="title-2kq">
                            <h5 class="mb-0">
                                <p class="fw-500 text-black">
                                    Đối với vé trúng 2 kết quả
                                </p>
                            </h5>
                        </div>

                        <div id="ve-trung-2" class="collapse" aria-labelledby="title-2kq" data-parent="#accordion">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste suscipit, exercitationem
                                rerum distinctio iusto voluptate rem ipsam provident pariatur. Amet.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pointer px-0 py-2 collapsed" data-toggle="collapse"
                            data-target="#ve-trung-3" aria-expanded="true" aria-controls="ve-trung-3" id="title-3kq">
                            <h5 class="mb-0">
                                <p class="fw-500 text-black">
                                    Đối với vé trúng 3 kết quả
                                </p>
                            </h5>
                        </div>

                        <div id="ve-trung-3" class="collapse" aria-labelledby="title-3kq" data-parent="#accordion">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste suscipit, exercitationem
                                rerum distinctio iusto voluptate rem ipsam provident pariatur. Amet.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pointer px-0 py-2 collapsed" data-toggle="collapse"
                            data-target="#ve-trung-4" aria-expanded="true" aria-controls="ve-trung-4" id="title-4kq">
                            <h5 class="mb-0">
                                <p class="fw-500 text-black">
                                    Đối với vé trúng 4 kết quả
                                </p>
                            </h5>
                        </div>

                        <div id="ve-trung-4" class="collapse" aria-labelledby="title-4kq" data-parent="#accordion">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste suscipit, exercitationem
                                rerum distinctio iusto voluptate rem ipsam provident pariatur. Amet.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pointer px-0 py-2 collapsed" data-toggle="collapse"
                            data-target="#ve-trung-5" aria-expanded="true" aria-controls="ve-trung-5" id="title-5kq">
                            <h5 class="mb-0">
                                <p class="fw-500 text-black">
                                    Đối với vé trúng 5 kết quả
                                </p>
                            </h5>
                        </div>

                        <div id="ve-trung-5" class="collapse" aria-labelledby="title-5kq" data-parent="#accordion">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste suscipit, exercitationem
                                rerum distinctio iusto voluptate rem ipsam provident pariatur. Amet.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pointer px-0 py-2 collapsed" data-toggle="collapse"
                            data-target="#ve-trung-6" aria-expanded="true" aria-controls="ve-trung-6" id="title-6kq">
                            <h5 class="mb-0">
                                <p class="fw-500 text-black">
                                    Đối với vé trúng 6 kết quả
                                </p>
                            </h5>
                        </div>

                        <div id="ve-trung-6" class="collapse" aria-labelledby="title-6kq" data-parent="#accordion">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste suscipit, exercitationem
                                rerum distinctio iusto voluptate rem ipsam provident pariatur. Amet.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pointer px-0 py-2 collapsed" data-toggle="collapse"
                            data-target="#ve-trung-7" aria-expanded="true" aria-controls="ve-trung-7" id="title-7kq">
                            <h5 class="mb-0">
                                <p class="fw-500 text-black">
                                    Đối với vé trúng 7 kết quả
                                </p>
                            </h5>
                        </div>

                        <div id="ve-trung-7" class="collapse" aria-labelledby="title-7kq" data-parent="#accordion">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste suscipit, exercitationem
                                rerum distinctio iusto voluptate rem ipsam provident pariatur. Amet.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pointer px-0 py-2 collapsed" data-toggle="collapse"
                            data-target="#ve-trung-8" aria-expanded="true" aria-controls="ve-trung-8" id="title-8kq">
                            <h5 class="mb-0">
                                <p class="fw-500 text-black">
                                    Đối với vé trúng 8 kết quả
                                </p>
                            </h5>
                        </div>

                        <div id="ve-trung-8" class="collapse" aria-labelledby="title-8kq" data-parent="#accordion">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste suscipit, exercitationem
                                rerum distinctio iusto voluptate rem ipsam provident pariatur. Amet.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pointer px-0 py-2 collapsed" data-toggle="collapse"
                            data-target="#ve-trung-9" aria-expanded="true" aria-controls="ve-trung-9" id="title-9kq">
                            <h5 class="mb-0">
                                <p class="fw-500 text-black">
                                    Đối với vé trúng 9 kết quả
                                </p>
                            </h5>
                        </div>

                        <div id="ve-trung-9" class="collapse" aria-labelledby="title-9kq" data-parent="#accordion">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste suscipit, exercitationem
                                rerum distinctio iusto voluptate rem ipsam provident pariatur. Amet.</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header pointer px-0 py-2 collapsed" data-toggle="collapse"
                            data-target="#ve-trung-10" aria-expanded="true" aria-controls="ve-trung-10" id="title-10kq">
                            <h5 class="mb-0">
                                <p class="fw-500 text-black">
                                    Đối với vé trúng 10 kết quả
                                </p>
                            </h5>
                        </div>

                        <div id="ve-trung-10" class="collapse" aria-labelledby="title-10kq" data-parent="#accordion">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste suscipit, exercitationem
                                rerum distinctio iusto voluptate rem ipsam provident pariatur. Amet.</p>
                        </div>
                    </div>
                </div>
                <div class="mb-3 mt-3">
                    <p> Ngoài ra. bạn có thể chọn mức cược cho từng vé. Số tiền bạn nhận được ở mỗi vé khi trúng giải sẽ
                        được nhân lên với số mức cược và mức cược tối đa là 50.000 VND.</p>
                    <p>VD: Bạn chơi 1 vé ở bậc 5, vé của bạn sẽ có 5 số, vé bạn trùng khớp 3 số với kết quả => bạn trúng
                        giải
                        10.000đ. Mức cược bạn chọn là 50.000đ => tổng giải thưởng bạn nhận được là 10.000 x 5= 50.000
                        VND
                    </p>
                </div>

                <div class="mb-3">
                    <p>Riêng đối với bậc 10, giải thưởng tối đa là 10 tỷ, giải thưởng sẽ được chia cho tất cả những vé
                        trùng
                        khớp 10 số với kết quả (tùy theo từng mức cược của từng vé)</p>
                    <p>VD: Có 6 vé trúng giải 2 tỷ, trong đó 1 vé có mức cược là 50.000, 2 vé có mức cược là 20.000 và 3
                        vé
                        có
                        mức cược là 10.000, vậy 10 tỷ sẽ được chia như sau:</p>
                </div>
                <div class="text-center fw-500 fz-18">
                    <p> 10 tỷ / [(1 vé 50.000 <=> 5) + (2 vé 20.000 <=> 4) + (3 vé 10.000 <=> 3) = 10 tỷ / 12 =</p>
                    <p>~833tr/10.000 đã đặt cược</p>
                </div>
                <div class="text-center fz-14">
                    <p>Vé cược 50.000đ => tổng giải thưởng nhận được 4,165 tỷ</p>
                    <p>Vé cược 20.0000đ => tổng giải thưởng nhận được là 1,666 tỷ</p>
                    <p>Vé cược 10.000đ => tông giải thưởng nhận được là 833 triệu</p>
                </div>
            </div>
            <!-- end co cau giai thuong -->

            <!-- xem ket qua -->
            <div id="xem-ket-qua">
                <h3>Xem kết quả</h3>
                <div class="row">
                    <div class="col-md-6">
                        <p class="fw-500 mb-2"> Cách 1</p>
                        <p> Bước 1: Truy cập vào link <a href="www.xosotuchon.com/keno/ketqua"
                                class="text-or">www.xosotuchon.com/keno/ketqua</a></p>
                        <p>Bước 2: Chọn ngày muốn xem</p>
                        <p> Bước 3: Chọn kỳ quay muốn xem</p>
                        <p>Bước 4: Chọn XEM KẾT QUẢ</p>
                    </div>
                    <div class="col-md-6">
                        <div class="fw-500 mb-2"> Cách 2</div>
                        <p> Đến đại lý Vietlott để xem kết quả</p>
                    </div>
                </div>
            </div>
            <!-- end xem ket qua -->
            <div id="nhan-thuong">
                <!-- nhan thuong -->
                <h3>Nhận thưởng</h3>
                <p>Với giải thưởng từ 50 tỷ trở xuống, bạn có thể đổi thưởng và nhận trực tiếp tại đại lý của
                    Vietlott.
                </p>
                <p>Với giải thưởng trên 50 tỷ, bạn đổi thưởng trực tiếp tại công ty Vietlott.</p>
                <p>Lưu ý: thời gian đổi thưởng là 60 ngày sau khi công bố kết quả. Sau thời gian này, giải thưởng sẽ
                    không
                    được nhận nữa và...</p>
            </div>
            <!-- end nhan thuong -->

            <!-- cach choi -->
            <div id="cach-choi">
                <h3>Cách chơi</h3>
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus, temporibus tenetur debitis quia
                laborum
                a ea voluptatibus placeat expedita tempore similique sapiente vitae eos libero alias iusto maiores. Sit
                omnis, a aspernatur totam deleniti asperiores ullam sint ex nulla est iste quas. Voluptates hic harum
                nihil,
                obcaecati odit praesentium in.
            </div>
            <!-- end cach choi -->
        </div>
    </div>
</div>

<?php include('./include/footer.php')  ?>