<?php include('./include/header.php') ?>
<div class="container responsibility py-2">

    <!-- start row -->
    <div class="row">
        <div class="col-lg-8">
            <h2 class="text-or title_primary fz-24">Chơi có trách nhiệm</h2>
            <h1>Vietlott tiên phong trong chương trình “Chơi có trách nhiệm”</h1>
            <p class="date_txt"><i class="fas fa-calendar mr-1"></i>
                <span>17/04/2019 09:51</span></p>
            <strong>
                Là doanh nghiệp tiên phong trong việc ứng dụng công nghệ hiện đại vào kinh doanh xổ số từ khâu phát hành
                vé, quay số mở thưởng và trả thưởng cho khách hàng, Vietlott đồng thời cũng là doanh nghiệp đi đầu trong
                việc triển khai chương trình “Chơi có trách nhiệm” nhằm đảm bảo người chơi tham gia dự thưởng một cách
                hợp pháp, lành mạnh và tránh được những rủi ro về tài chính
            </strong>
            <img src="./assets/images/news/vietlote.png" alt="">
            <h3>“Chơi có trách nhiệm” là gì?</h3>
            <p>Trong hơn 2 năm kinh doanh (từ 18/07/2018), Vietlott đã lần lượt cho ra mắt các sản phẩm xổ số tự chọn số
                điện toán như Mega 6/45, Max 4D và Power 6/55 với cách chơi hiện đại cùng nhiều giải thưởng hấp dẫn. Với
                giải thưởng lớn, cách chơi hiện đại và hệ thống đại lý, điểm bán hàng phủ khắp 49 tỉnh thành trên cả
                nước, xổ số tự chọn số điện toán Vietlott đã thu hút được sự quan tâm của đông đảo người dân và dư luận
                xã hội. Bên cạnh các hoạt động kinh doanh, Vietlott cũng thực hiện nhiều chương trình từ thiện, an sinh
                xã hội. Đặc biệt, Vietlott là doanh nghiệp đi đầu trong việc triển khai chương trình “Chơi có trách
                nhiệm” nhằm đảm bảo người chơi tham gia dự thưởng một cách hợp pháp, lành mạnh và tránh được những rủi
                ro về tài chính.</p>
            <p>Chơi có trách nhiệm là một chương trình gồm 4 cấp độ mà các thành viên của WLA cam kết thực hiện để đảm
                bảo quyền lợi của người chơi và cộng đồng vì mục tiêu phát triển thị trường xổ số theo hướng lành mạnh,
                bền vững. Là doanh nghiệp xổ số duy nhất tại Vietnam tham gia Hiệp hội Xổ số Châu Á Thái Bình Dương
                (APLA) và Hiệp hội xổ số Thế giới (WLA), Vietlott hoạt động tuân thủ các chuẩn mực nghiêm ngặt nhất của
                WLA về trách nhiệm xã hội, chơi có trách nhiệm, quản lý rủi ro và an toàn trong kinh doanh xổ số. Theo
                đó, Vietlott không cung cấp các sản phẩm cho người chơi dưới 18 tuổi, người bị hạn chế về năng lực hành
                vi dân sự hoặc mất năng lực dân sự theo quy định của pháp luật. Đồng thời, Vietlott cũng đưa ra những
                khuyến cáo giúp người chơi tránh được những rủi ro về tài chính khi tham gia dự thưởng.
            </p>
            <ol>
                <li>Bạn đang mua xổ số để giải trí, không phải đầu tư tiền của bạn</li>
                <li> Trước khi chơi, hãy đặt giới hạn số tiền và thời gian mà bạn sẽ tham gia</li>
                <li>Bỏ chơi khi bạn quá kích động</li>
                <li> Đừng bỏ quá nhiều tiền vào các trò chơi có thưởng với hy vọng sẽ lấy lại số tiền mà bạn đã mất</li>
                <li> Hãy giữ các thú vui và thói quen khác, đừng tham gia trò chơi có thưởng suốt cuộc đời</li>
                <li>Đừng tham gia chơi để giảm stress hoặc buồn phiền</li>
                <li>Tham gia các trò chơi có thưởng vừa phải là cần thiết</li>
                <li>Điều quan trọng nếu bạn chơi, hãy chơi có trách nhiệm</li>
            </ol>
            <h3>Trách nhiệm của Vietlott trong việc triển khai chương trình “Chơi có trách nhiệm”
            </h3>

            <p>“Chơi có trách nhiệm” là thông điệp mà nhiều công ty xổ số trên thế giới, trong đó có Vietlott mong
                muốn gửi đến người chơi, theo đó khuyến cáo người chơi tham gia dự thưởng tùy theo năng lực tài
                chính của bản thân, không chơi vượt quá khả năng kinh tế. Không chỉ thực hiện “trách nhiệm” với
                người chơi, Vietlott còn thực hiện nhiều hoạt động thể hiện trách nhiệm với cộng đồng, chia sẻ “cơ
                hội để tốt hơn” đến với mỗi người trong xã hội.</p>
            <img src="./assets/images/news/vietlote-2.png" alt="">
            <p>Tính đến tháng 10/2018, Vietlott đã đóng góp hơn 2.500 tỷ đồng cho ngân sách các địa phương để đầu tư
                nâng cấp hạ tầng cơ sở trong lĩnh vực y tế, giáo dục và công trình phúc lợi xã hội, thực hiện trả thưởng
                cho người chơi với tổng giá trị hơn 4.875 tỷ đồng, tạo ra hơn 8.000 việc làm cho lao động tại các địa
                phương có điểm bán hàng. Đặc biệt, với mỗi 10.000 đồng người chơi mua vé xổ số tự chọn, Vietlott sẽ
                trích 3.000 đồng cho các hoạt động an sinh xã hội, từ thiện. Cùng với các đối tác, hệ thống đại lý, điểm
                bán hàng và các khách hàng may mắn trúng thưởng, Vietlott đã trao tặng hàng chục ngôi nhà tình nghĩa,
                hàng nghìn phần học bổng, quà tặng cho trẻ em khó khăn, người nghèo, hỗ trợ viện phí cho bệnh nhi…</p>
            <p>Với sự tin tưởng và ủng hộ của khách hàng, trong năm 2019, Vietlott sẽ tiến tới mở rộng kinh doanh trên
                toàn quốc, tiếp tục thực hiện sứ mệnh phát triển thị trường trò chơi giải trí có thưởng tại Việt Nam
                theo hướng hiện đại, minh bạch và có trách nhiệm theo 3 giá trị cốt lõi: Trung thực (Trung thực trong
                suy nghĩ và hành động), Trách nhiệm (Trách nhiệm trong quyết định và thực thi nhiệm vụ nhằm mang đến cơ
                hội để tốt hơn cho cộng đồng) và Tôn trọng (Tôn trọng bản thân, đồng nghiệp và khách hàng)</p>
        </div>
        <div class="col-lg-4 related-news">
            <h2 class="text-or title_primary fz-24">Tin liên quan</h2>
            <!-- start news -->
            <div class="card">
                <div>
                    <img src="./assets/images/news/1.png" alt="" class="img-fluid">
                </div>
                <div class="card-body">
                    <h4 class="card-title">Tổng hợp giải Jackpot của Mỹ</h4>
                    <p class="date_txt">
                        <i class="fas fa-calendar mr-1"></i>
                        25/06/2019 06:47
                    </p>
                </div>
            </div>
            <!-- end news -->
            <!-- start news -->
            <div class="card">
                <div>
                    <img src="./assets/images/news/2.png" alt="" class="img-fluid">
                </div>
                <div class="card-body">
                    <h4 class="card-title">Infographic 9 lưu ý "chơi có trách nhiệm"</h4>
                    <p class="date_txt">
                        <i class="fas fa-calendar mr-1"></i>
                        25/06/2019 06:47
                    </p>
                </div>
            </div>
            <!-- end news -->
            <!-- start news -->
            <div class="card">
                <div>
                    <img src="./assets/images/news/3.png" alt="" class="img-fluid">
                </div>
                <div class="card-body">
                    <h4 class="card-title">Nên làm gì nếu bỗng dưng trúng xổ số độc đắc?</h4>
                    <p class="date_txt">
                        <i class="fas fa-calendar mr-1"></i>
                        25/06/2019 06:47
                    </p>
                </div>
            </div>
            <!-- end news -->
            <!-- start news -->
            <div class="card">
                <div>
                    <img src="./assets/images/news/4.png" alt="" class="img-fluid">
                </div>
                <div class="card-body">
                    <h4 class="card-title">Những giải thưởng xổ số lớn nhất thế giới</h4>
                    <p class="date_txt">
                        <i class="fas fa-calendar mr-1"></i>
                        25/06/2019 06:47
                    </p>
                </div>
            </div>
            <!-- end news -->
            <!-- start news -->
            <div class="card">
                <div>
                    <img src="./assets/images/news/5.png" alt="" class="img-fluid">
                </div>
                <div class="card-body">
                    <h4 class="card-title">Hãy chơi có trách nhiệm</h4>
                    <p class="date_txt">
                        <i class="fas fa-calendar mr-1"></i>
                        25/06/2019 06:47
                    </p>
                </div>
            </div>
            <!-- end news -->
        </div>
    </div>
    <!-- end row -->
</div>

<?php include('./include/footer.php')  ?>