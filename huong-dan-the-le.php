<?php include('./include/header.php') ?>
<div class="container rulers py-2">
    <h1>Hướng dẫn & thể lệ</h1>
    <!-- start row -->
    <div class="row">
        <div class="col-md-4 d-none d-md-block">
            <div class="list-group" id="menu_item">
                <a href="#cach-thuc" class="list-group-item list-group-item-action active">Cách thức tham gia</a>
                <a href="#co-cau-giai-thuong" class="list-group-item list-group-item-action">Cơ cấu giải thưởng</a>
                <a href="#cach-thuc-choi-game" class="list-group-item list-group-item-action">Cách thức chơi game</a>
                <a href="#cach-thuc-nhan-thuong" class="list-group-item list-group-item-action">Cách thức nhận
                    thưởng</a>

            </div>
        </div>
        <div class="col-md-8 content_rule">
            <div id="cach-thuc">
                <h3 style="margin-top: 8px !important">Cách thức tham gia</h3>
                <p><span class="fw-600">Bước 1</span>: Đăng nhập vào website <a href="www.xosotuchon.com/keno"
                        class="text-or">www.xosotuchon.com/keno</a></p>
                <p> Bạn có thể đăng nhập website bằng nhiều cách thức sau:</p>
                <ul>
                    <li> - Vào trình duyệt web và gõ địa chỉ <a href="www.xosotuchon.com/keno"
                            class="text-or">www.xosotuchon.com/keno</a> </li>
                    <li> - Truy cập vào từ trang web wifi của các đối tác của Keno như Highland, AWing Wifi </li>
                    <li> - Truy cập vào từ app của các đối tác Keno như Grab</li>
                </ul>

                <p><span class="fw-600">Bước 2</span>: Click chọn “<span class="fw-600 fz-16 text-or">CHƠI NGAY</span>”
                    tại banner Game Keno</p>
                <p>
                    <span class="fw-600">Bước 3:</span> Điền thông tin Tên người dùng, mật khẩu, số điện thoại và nhập
                    mã OTP để tham gia trải
                    nghiệm
                    ngay
                </p>
            </div>
            <!-- co cau giai thuong -->
            <div id="co-cau-giai-thuong">
                <h3>Cơ cấu giải thưởng</h3>
                <p>Thời gian diễn ra Game là <span class="fw-600 text-or">23.08.2019 - 22.09.2019</span></p>
                <p>Mỗi ngày bạn sẽ có <span class="txt_strong">50.000đ/sđt/ngày</span> để chơi Game</p>
                <ul>
                    <li> - Bạn đặt cược ở mức <span class="txt_strong">10.000 Đ</span> => Khi trúng ở bất cứ giải
                        thưởng nào bạn sẽ nhận được <span class="txt_strong">10.000 Đ</span>
                    </li>
                    <li> - Bạn đặt cược ở mức <span class="txt_strong">20.000 Đ</span> => Khi trúng ở bất cứ giải
                        thưởng nào bạn sẽ nhận được <span class="txt_strong">20.000 Đ</span>
                    </li>
                    <li> - Bạn đặt cược ở mức <span class="txt_strong">30.000 Đ</span> => Khi trúng ở bất cứ giải
                        thưởng nào bạn sẽ nhận được <span class="txt_strong">30.000 Đ</span>
                    </li>
                    <li> - Bạn đặt cược ở mức <span class="txt_strong">40.000 Đ</span> => Khi trúng ở bất cứ giải
                        thưởng nào bạn sẽ nhận được <span class="txt_strong">40.000 Đ</span>
                    </li>
                    <li> - Bạn đặt cược ở mức <span class="txt_strong">50.000 Đ</span> => Khi trúng ở bất cứ giải
                        thưởng nào bạn sẽ nhận được <span class="txt_strong">50.000 Đ</span>
                    </li>
                </ul>

                <p><span class="fw-600">Bước 2</span>: Click chọn “<span class="fw-600 fz-16 text-or">CHƠI NGAY</span>”
                    tại banner Game Keno</p>
                <p>
                    <span class="fw-600">Bước 3:</span> Điền thông tin Tên người dùng, mật khẩu, số điện thoại và nhập
                    mã OTP để tham gia trải
                    nghiệm
                    ngay
                </p>
            </div>
            <!-- end co cau giai thuong -->

            <!-- cach thuc choi game -->
            <div id="cach-thuc-choi-game">
                <h3>Cách thức chơi game</h3>
                <h4>Bước 1: Chọn kỳ</h4>
                <p>Tại màn hình Game, bạn có thể chọn kỳ chơi bất kỳ trong 92 kỳ của nagfy, chỉ cần kỳ mà bạn chọn chưa
                    được quay số </p>
                <img src="./assets/images/news/img_placeholder.png" alt="" class="img-fluid">
                <h4> Bước 2: Chọn bậc chơi</h4>
                <p>
                    Tại màn hình Game sẽ có 10 bậc chơi để bạn có thể chọn. Với mỗi bậc bạn sẽ được chọn số lượng
                    số tương ứng.
                </p>
                <p>
                    VD:
                </p>
                <p>
                    Bạn chọn chơi bậc 1, thì bạn sẽ được click chọn 1 số bất kỳ trong 80 số hiển thị trên màn hình
                </p>
                <p>
                    Bạn chọn chơi bậc 2, thì bạn sẽ được click chọn 2 số bất kỳ trong 80 số hiển thị trên màn hình
                </p>
                <p>
                    Bạn chọn chơi bậc 10, thì bạn sẽ được click chọn 10 số bất kỳ trong 80 số hiển thị trên màn hình
                </p>
                <img src="./assets/images/news/img_placeholder.png" alt="" class="img-fluid">

                <h4> Bước 3: Chọn mức cá cược</h4>
                <p>
                    Tại màn hình Game sẽ có 5 mức cá cược để bạn có thể chọn. Với mỗi mức cá cược bạn chọn sẽ tương ứng
                    với số lượng giải thưởng nhận được khi bạn trúng giải. (xem Cơ cấu giải thưởng).
                </p>
                <img src="./assets/images/news/img_placeholder.png" alt="" class="img-fluid">

                <h4> Bước 4: Chọn dãy số cho vé</h4>
                <p>
                    Tại màn hình Game sẽ hiển thị 80 số để bạn chọn, với từng bậc chơi bạn đã chọn ở bước 1, bạn sẽ được
                    chọn số lượng số tương ứng với mỗi bậc. Bạn click vào các số mà mình muốn chọn. Dãy số bạn
                    chọn</span> sẽ
                    hiển
                    thị ở khu vực thông tin “<span class="text-uppercase txt_strong fz-16">Dãy số bạn chọn</span>”. Sau
                    đó click “<span class="text-uppercase txt_strong fz-16">Xác nhận Bộ số</span>” để hoàn thành 1 lượt
                    đặt
                    vé.
                </p>
                <img src="./assets/images/news/img_placeholder.png" alt="" class="img-fluid">

            </div>
            <!-- end cach thuc choi game -->
            <!-- cach thuc nhan thuong -->
            <div id="cach-thuc-nhan-thuong">
                <h3>Cách thức nhận thưởng</h3>

                <p>Sau khi nhận được mã code, bạn hãy đến các địa điểm bán Vietlott để quy đổi ra vé Keno
                    và trải nghiệm thật Loại sản phẩm mới này của Vietlott. Các bạn sẽ được đại lý Vietlott hướng dẫn
                    tận tình các
                    quy trình để nhận giải thướng và cách chơi thật Keno tại đại lý.
                    Mã QR code sẽ được lưu trong mục <a href="#" class="txt_strong">Thông tin tài khoản của bạn</a></p>

                <p style="font-style: italic; margin-top: 20px;">Lưu ý: thời gian đổi thưởng của mã QR Code là <span class="txt_strong">3 ngày sau khi công bố kết
                        quả</span>. Sau thời gian này giải
                    thưởng
                    sẽ hết hiệu lực.</p>
            </div>
            <!-- end cach thuc nhan thuong -->
        </div>
    </div>
</div>

<?php include('./include/footer.php')  ?>