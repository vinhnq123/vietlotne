<?php include('./include/header.php') ?>
<div class="container contact py-2 overflow-auto">
    <h2>Thông tin liên hệ</h2>
    <div class="row mt-4">
        <div class="col-md-4">
            <div class="p-3 shadow bg-white">
                <h3 class="fz-24 fw-600">Dành cho khách hàng mua sản phẩm</h3>
                <p class="fw-500">Email: <span class="text-or">contact@vietlott.vn</span></p>
                <p class="fw-500">Hotline: <span class="text-or">1xx xxx xxxx</span></p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="p-3 shadow bg-white">
                <h3 class="fz-24 fw-600">Dành cho khách hàng muốn kinh doanh Vietlott</h3>
                <p class="fw-500">Email: <span class="text-or">contact@vietlott.vn</span></p>
                <p class="fw-500">Hotline: <span class="text-or">1xx xxx xxxx</span></p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="p-3 shadow bg-white">
                <h3 class="fz-24 fw-600">Dành cho khách hàng muốn tổ chức game KENO</h3>
                <p class="fw-500">Email: <span class="text-or">contact@vietlott.vn</span></p>
                <p class="fw-500">Hotline: <span class="text-or">1xx xxx xxxx</span></p>
            </div>
        </div>
    </div>
    <h2>Câu hỏi thường gặp</h2>
    <div class="row mt-4">
        <div class="col-md-6">
            <div class="p-3 shadow bg-white d-flex">
                <div class="prefix"></div>
                <div class="txt">
                    <h3 class="fz-16">Tôi không có thiết bị kết nối web, tôi có thể chơi thử Game Keno ở đâu?</h3>
                    <p>Bạn có thể đến các điểm bán hàng của Keno để chơi thử và sẽ có đội ngũ nhân viên của chúng tôi
                        hướng dân tận tính cho bạn nhé.</p>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="p-3 shadow bg-white d-flex">
                <div class="prefix"></div>
                <div class="txt">
                    <h3 class="fz-16">Tôi có thể đổi QR CODE ở đâu?</h3>
                    <p>Bạn có thể đến các địa điểm bán hàng của Vietlott, bạn có thể đổi giải thưởng nhé.</p>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="p-3 shadow bg-white d-flex">
                <div class="prefix"></div>
                <div class="txt">
                    <h3 class="fz-16">Thời hạn đổi thưởng chơi game là như thế nào?</h3>
                    <p>Bạn có quyền đổi thưởng trong vòng 3 ngày kể từ ngày nhận QR CODE.</p>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="p-3 shadow bg-white d-flex">
                <div class="prefix"></div>
                <div class="txt">
                    <h3 class="fz-16">Tôi trúng vé số, nhưng nếu tôi mất giấy tờ tùy thân chỉ còn có giấy khai sinh và sổ hộ khẩu thì có nhận giải được không?</h3>
                    <p>Bạn chỉ cần dùng giấy tờ xác minh nào có hình anh bạn là được nhé.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('./include/footer.php')  ?>