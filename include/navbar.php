<nav class="navbar navbar-light menu_top bg-light mb-5 px-2">
	<!-- navbar-expand-sm  -->
	<!-- <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId"
		aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button> -->
	<div class="d-flex flex-wrap justify-content-around menu_top p-0 col-12" id="collapsibleNavId">
		<div class="search_box col-md-7">
			<form class="form-inline my-2 my-lg-0 position-relative w-100">
				<input class="form-control mr-sm-2 search_input w-100" type="text">
				<button class="btn btn-search my-sm-0" type="submit"><i class="fas fa-search"></i></button>
			</form>
		</div>
		<div class="d-flex ml-auto flex-wrap col-md-5 menu_left align-items-center">
			<button class="btn btn-light" id="menu-toggle"><i class="fas fa-bars"></i></button>
			<img src="./assets/images/common/Logo.png" class="ml-1 lg-primary" alt="logo vietlote">
			<!-- <button class="btn btn-user btn-signup mr-2">Đăng ký</button>
			<button class="btn btn-user btn-login mr-2">Đăng nhập</button> 
			<img src="./assets/images/common/Vietlott_logo.png" class="ml-1 lg-vietlote" alt="logo vietlote">-->


			<!-- if user login -->
			<div class="btn-group">
				<div class="d-flex align-items-center mr-3">
					<i class="far fa-bell fz-20 pointer" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">
						<div class="alert_count">29</div>
					</i>
					<div class="dropdown-menu dropdown-menu-right dropdown-alert alert_custom">
						<a class="dropdown-item text-truncate-2" href="#">Lorem ipsum, dolor sit amet
							consectetur adipisicing elit. Commodi eius quidem</a>
						<a class="dropdown-item text-truncate-2" href="#">Lorem, ipsum dolor sit amet consectetur
							adipisicing elit. Impedit quo quasi rerum sunt suscipit et earum quae reprehenderit nam.
							Maiores.</a>
						<a class="dropdown-item text-truncate-2" href="#">Lorem, ipsum dolor sit amet consectetur
							adipisicing elit. Impedit quo quasi rerum sunt suscipit et earum quae reprehenderit nam.
							Maiores.</a>
					</div>
				</div>
				<div class="btn-group">
					<i class="far fa-user fz-20 pointer" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"></i>
					<div class="dropdown-menu dropdown-menu-right dropdown-alert alert_custom">
						<a class="dropdown-item text-truncate-2" href="#">Thông tin tài khoản</a>
						<a class="dropdown-item text-truncate-2" href="#">Nhật ký hoạt động</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item text-truncate-2" href="#">Đăng xuất</a>
					</div>
				</div>
			</div>
			<!-- end if user login -->
			<img src="./assets/images/common/Vietlott_logo.png" class="ml-1 lg-vietlote" alt="logo vietlote" />
		</div>
	</div>
</nav>