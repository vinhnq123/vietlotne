<!-- Sidebar -->
<div class="bg-light" id="sidebar-wrapper">
  <div class="sidebar-heading">
    <a href="#">
      <img src="./assets/images/common/Logo.png" alt="Keno xổ số tự chọn" />
    </a>
  </div>
  <div class="list-group list-group-flush">
    <a
      href="#"
      class="list-group-item list-group-item-action text-white menu-active"
      >Trang chủ</a
    >
    <a href="#" class="list-group-item list-group-item-action text-white"
      >Giới thiệu</a
    >
    <a href="#" class="list-group-item list-group-item-action text-white"
      >Trải nghiệm Keno</a
    >
    <a href="#" class="list-group-item list-group-item-action text-white"
      >Keno cùng người nổi tiếng</a
    >
    <a href="#" class="list-group-item list-group-item-action text-white"
      >Tìm điểm bán</a
    >
    <a href="#" class="list-group-item list-group-item-action text-white"
      >Trở thành đại lý</a
    >
    <a href="#" class="list-group-item list-group-item-action text-white"
      >Các sản phẩm khác</a
    >
    <a href="#" class="list-group-item list-group-item-action text-white"
      >Liên hệ</a
    >
    <a href="#" class="list-group-item list-group-item-action text-white"
      >Chơi có trách nhiệm</a
    >
    <a href="#" class="list-group-item list-group-item-action text-white"
      >Đối tác</a
    >
  </div>
  <div class="social d-flex justify-align-center align-items-center">
    <div class="icon_fb">
      <img src="./assets/images/icons/fb.png" />
      <div class="tip tip-fb">
        <i class="icon_tip icon_speak"></i>
        <div class="txt_speak fw-600 fz-15">
          Chia sẻ để nhận thêm <span class="text-or">10K</span> chơi game
        </div>
      </div>
    </div>
    <div class="icon_zalo">
      <img src="./assets/images/icons/zalo.png" />
      <div class="tip tip-zalo">
        <i class="icon_tip icon_speak"></i>
        <div class="txt_speak fw-600 fz-15">
          Chia sẻ để nhận thêm <span class="text-or">10K</span> chơi game
        </div>
      </div>
    </div>
	<div class="icon_youtube">
		<img src="./assets/images/icons/youtube.png" />
		<div class="tip tip-youtube">
		  <i class="icon_tip icon_speak"></i>
		  <div class="txt_speak fw-600 fz-15">
			Chia sẻ để nhận thêm <span class="text-or">10K</span> chơi game
		  </div>
		</div>
	  </div>
  </div>
</div>
<!-- /#sidebar-wrapper -->
