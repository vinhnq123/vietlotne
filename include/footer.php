
<footer id="footer">
	<div class="container">
		<img src="./assets/images/common/vietlott_logo_footer.png" alt="" class="d-block mx-auto">
		<ul class="menu flex-wrap">
			<li><a href="#">Giới thiệu</a></li>
			<li><a href="#">Trải nghiệm Keno</a></li>
			<li><a href="#">Keno cùng người nổi tiếng</a></li>
			<li><a href="#">Tìm điểm bán</a></li>
			<li><a href="#">Trở thành đại lý</a></li>
			<li><a href="#">Các sản phẩm khác</a></li>
			<li><a href="#">Liên hệ</a></li>
			<li><a href="#">Chơi có trách nhiệm</a></li>
			<li><a href="#">Đối tác</a></li>
		</ul>
		<div class="row mt-4">
			<div class="col-md-3 ">
				<div class="box_footer">
					<div class="line">
						<div class="image_prefix img_18"></div>
						<p>Dành cho người trên 18 tuổi</p>
					</div>
					<div>
						<div class="line">
							<div class="image_prefix img_secu"></div>
							<p>Chơi game có trách nhiệm</p>
						</div>
					</div>
				</div>
			</div>
				<div class="col-md-3 ">
					<div class="box_footer">
						<img src="./assets/images/common/aplt.png" class="img-fuid d-block mx-auto" />
						<p class="title_organize">Thành viên Hiệp hội Xổ số Châu Á Thái Bình Dương</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="box_footer">
						<img src="./assets/images/common/wla.png" class="img-fuid d-block mx-auto" />
						<p class="title_organize">Thành viên Hiệp hội Xổ số thế giới (WLA)</p>
					</div>
				</div>
				<div class="col-md-3 ">
					<div class="box_footer">
						<img src="./assets/images/common/WLA_world.png" class="img-fuid d-block mx-auto" />
						<p class="title_organize">Chứng chỉ Level 2</p>
						<p class="title_organize">Chơi game trách nhiệm của WLA</p>
					</div>
				</div>
			</div>
			<hr/>
			<div class="row justify-content-between py-3 mt-4">
				<div class="col-md-6 txt_footer">
					<p>© 2018, Công ty Xổ Số Điện Toán Việt Nam - Bộ Tài Chính.</p>
					<p>Giấy phép: Số 3380/GP-TTĐT do Sở Thông tin và Truyền thông Hà Nội cấp ngày 30/8/2018</p>
					<p>Người chịu trách nhiệm: Tổng Giám Đốc Nguyễn Thanh Đạm.</p>
				</div>
				<div class="col-md-6 txt_footer">
					<p>Địa chỉ : Tầng 15, Tòa nhà CornerStone, 16 Phan Chu Trinh, Quận Hoàn Kiếm, Hà Nội</p>
					<p>Tổng đài Chăm sóc khách hàng: 1900.55.88.89</p>
					<p>Điện thoại: 024.62.686.818 - Email: contact@vietlott.vn</p>
				</div>
			</div>
		</div>
		<div class="back_to_top" id="toTop"></div>
</footer>

<!-- /#page-content-wrapper -->
</div>
<div class="layout-fixleft">
	<div class="tip_fixed">
        <i class="icon_award"></i>
        <div class="txt_award fw-600 text-or">
          ĐỔI ĐIỂM
        </div>
	</div>
	<img src="./assets/images/home/god-fixed.png" alt="" class="god_fixed mb-2">
	<button class="btn btn-fixed btn-call mb-2"><i class="fas fa-phone-alt"></i></button>
	<button class="btn btn-fixed btn-location"><span class="txt_vertical">Tìm điểm bán</span> <i
			class="fas fa-map-marker-alt"></i> </button>
</div>
<!-- <script src="./assets/js/jquery-3.3.1.slim.min.js"></script> -->
<script src="./assets/js/jquery-2.2.4.min.js"></script>
<script src="./assets/js/bootstrap.min.js"></script>
<script src="./assets/js/popper.min.js"></script>
<script src="./assets/js/custom.js"></script>
<script src="./assets/js/jquery.flipper-responsive.js"></script>
<script>
		jQuery(function ($) {
			$('#myFlipper').flipper('init');
			$('#modalFlipper').flipper('init');
		});
	</script>
</body>

</html>