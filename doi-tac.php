<?php include('./include/header.php') ?>
<div class="container partner py-2">
    <h1>Hướng dẫn & thể lệ</h1>
    <div class="row mt-4">
        <div class="col-md-4 text-center bg-white">
            <div class="p-4 shadow">
                <div class="type_partner">Đối tác Bạch kim</div>
                <img src="./assets/images/highland.png" alt="" class="img-fluid mt-3">
                <p class="title_partner">Highland Coffee</p>
                <a href="#" class="link_partner">Tìm hiểu thêm</a>
            </div>
        </div>

        <div class="col-md-8 text-center bg-white">
            <div class="p-4 shadow h-100">
                <div class="type_partner">Đối tác Vàng</div>
                <div class="d-flex align-items-center justify-content-center h-100">
                    <div class="d-flex align-items-center justify-content-between w-100">
                        <div class="item_partner">
                            <img src="./assets/images/grab.png" alt="" class="img-fluid mt-3 mb-4">
                            <p class="title_partner">Grab</p>
                            <a href="#" class="link_partner">Tìm hiểu thêm</a>
                        </div>

                        <div class="item_partner mt-2">
                            <img src="./assets/images/lazada.png" alt="" class="img-fluid mt-3 mb-4">
                            <p class="title_partner">Lazada</p>
                            <a href="#" class="link_partner">Tìm hiểu thêm</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center mt-4">
            <div class="p-4 shadow bg-white">
                <div class="type_partner">Đối tác Bạc</div>
                <div class="row">
                    <div class="col-md-3 col-6 text-center mt-4">
                        <img src="./assets/images/hoder.png" alt="" class="img-fluid"/>
                        <p class="title_partner">[Đối tác]</p>
                        <a href="#" class="link_partner">Tìm hiểu thêm</a>
                    </div>

                    <div class="col-md-3 col-6 text-center mt-4">
                        <img src="./assets/images/hoder.png" alt="" class="img-fluid"/>
                        <p class="title_partner">[Đối tác]</p>
                        <a href="#" class="link_partner">Tìm hiểu thêm</a>
                    </div>

                    <div class="col-md-3 col-6 text-center mt-4">
                        <img src="./assets/images/hoder.png" alt="" class="img-fluid"/>
                        <p class="title_partner">[Đối tác]</p>
                        <a href="#" class="link_partner">Tìm hiểu thêm</a>
                    </div>

                    <div class="col-md-3 col-6 text-center mt-4">
                        <img src="./assets/images/hoder.png" alt="" class="img-fluid"/>
                        <p class="title_partner">[Đối tác]</p>
                        <a href="#" class="link_partner">Tìm hiểu thêm</a>
                    </div>

                    <div class="col-md-3 col-6 text-center mt-4">
                        <img src="./assets/images/hoder.png" alt="" class="img-fluid"/>
                        <p class="title_partner">[Đối tác]</p>
                        <a href="#" class="link_partner">Tìm hiểu thêm</a>
                    </div>

                    <div class="col-md-3 col-6 text-center mt-4">
                        <img src="./assets/images/hoder.png" alt="" class="img-fluid"/>
                        <p class="title_partner">[Đối tác]</p>
                        <a href="#" class="link_partner">Tìm hiểu thêm</a>
                    </div>

                    <div class="col-md-3 col-6 text-center mt-4">
                        <img src="./assets/images/hoder.png" alt="" class="img-fluid"/>
                        <p class="title_partner">[Đối tác]</p>
                        <a href="#" class="link_partner">Tìm hiểu thêm</a>
                    </div>

                    <div class="col-md-3 col-6 text-center mt-4">
                        <img src="./assets/images/hoder.png" alt="" class="img-fluid"/>
                        <p class="title_partner">[Đối tác]</p>
                        <a href="#" class="link_partner">Tìm hiểu thêm</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php include('./include/footer.php')  ?>