<?php include('./include/header.php') ?>
<div class="container account_info">
  <h1 class="text-or title_primary ml-3">Thông tin tài khoản</h1>
  <div class="d-flex flex-wrap mt-4">
    <div class="col-md-4">
      <div class="card shadow">
        <ul class="list-group list-group-flush">
          <li class="list-group-item">
            <p>Tên tài khoản</p>
            <p>xyz abc</p>
          </li>
          <li class="list-group-item">
            <p>Mật khẩu</p>
            <a href="#" class="fw-500"> Đổi mật khẩu </a>
          </li>
          <li class="list-group-item">
            <p>Số điện thoại</p>
            <p>09x xxx xxxx</p>
          </li>
          <li class="list-group-item">
            <p>Điểm</p>
            <p>30 điểm</p>
          </li>
          <li class="list-group-item">
            <a href="#" class="btn btn-custom w-100 ">
              Đổi điểm
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="col-md-8 code_pro">
      <div id="accordion">
        <!-- code not used -->
        <div class="card shadow">
          <div class="card-header pointer p-3" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
            aria-controls="collapseOne" id="headingOne">
            <h5 class="mb-0">
              <p class="fw-500 fz-20 text-black">
                Code chưa dùng
              </p>
            </h5>
          </div>

          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body overflow-auto">
              <div class="">
                <div class="mt-3 code_promotion d-flex align-items-center">
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end code not used -->
        <!-- code used -->
        <div class="card shadow mt-3">
          <div class="card-header pointer p-3 collapsed" data-toggle="collapse" data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo" id="headingTwo">
            <h5 class="mb-0">
              <p class="fw-500 fz-20 text-black">
                Code đã dùng
              </p>
            </h5>
          </div>

          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body overflow-auto">
              <div class="">
                <div class="mt-3 code_promotion d-flex align-items-center">
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end code used -->
        <!-- start code expired -->
        <div class="card shadow mt-3">
          <div class="card-header pointer p-3" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true"
            aria-controls="collapseThree" id="headingThree">
            <h5 class="mb-0">
              <p class="fw-500 fz-20 text-black">
                Code hết hạn
              </p>
            </h5>
          </div>

          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="card-body overflow-auto">
              <div class="">
                <div class="mt-3 code_promotion d-flex align-items-center">
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                  <div class="px-2">
                    <img src="./assets/images/qrcode.png" alt="" class="img-fuild" />
                    <div class="mt-3">
                      <p class="fw-500 fz-14 text-82 mb-0">Giá trị</p>
                      <p class="fw-700 fz-24 text-or">30.000 Đ</p>
                      <p class="fw-500 fz-14 text-82 mt-2">Hạn sử dụng</p>
                      <p class="fw-500 fz-14 text-or">25.07.2019</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- enc code expired -->
      </div>
      <!-- <div class="fw-500 fz-20 text-black">Code chưa dùng</div>
      
      </div> -->
    </div>
  </div>
</div>
<?php include('./include/footer.php')  ?>