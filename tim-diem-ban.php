<?php include('./include/header.php') ?>
<div class="container agency">
	<h1 class="ml-4">Các điểm bán Keno</h1>
	<div class="row">
		<div class="col-md-12">
			<img src="./assets/images/banner-diem-ban.png" alt="" class="w-100 img-fluid d-none d-md-block">
			<img src="./assets/images/banner-diem-ban-mobile.png" alt="" class="img-fluid d-block d-md-none" />
		</div>
		<div class="col-md-6 mt-3">
			<div class="card">
				<img class="card-img-top" src="./assets/images/diem-ban-1.png" alt="" class="img-fluid">
				<div class="card-body">
					<h4 class="card-title">Màn hình minh hoạ trực quan tại mỗi điểm bán</h4>
					<p class="card-text">Từng bước tham gia <span class="text-or fw-500">KENO</span> sẽ được thể hiện
						một cách cụ thể, rõ ràng và vô cùng trực quan. Bạn có thể nắm rõ từng bước cụ thể và thao tác dễ
						dàng.</p>
				</div>
			</div>
		</div>
		<div class="col-md-6 mt-3">
			<div class="card">
				<img class="card-img-top" src="./assets/images/diem-ban-2.png" alt="" class="img-fluid">
				<div class="card-body">
					<h4 class="card-title">Đội ngũ PG hướng dẫn tận tình và chu đáo</h4>
					<p class="card-text">Trong lúc xem video hướng dẫn cách tham gia <span
							class="text-or fw-500">KENO</span>, đội ngũ PG sẽ hỗ sẽ bạn tận tình từng bước. Sau đó, PG
						sẽ giúp đỡ để bạn có thể mua vé ngay tại quầy bán hàng cho đến lúc bạn sở hữu tấm vé <span
							class="text-or fw-500">KENO</span> trên tay</p>
				</div>
			</div>
		</div>
		<div class="col-md-4 mt-3 map_picker ">
			<div class="title_map">Danh sách các điểm bán</div>
			<div class="px-3">

				<!-- //start map show is mobile -->
				<div class="map_mobile mt-3 d-block d-md-none">
					<iframe
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d52731.18416405924!2d106.61408225443789!3d10.790719578445556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752eec7de29523%3A0x5a82b9c14c594ff4!2zWOG7lSBz4buRIFZpZXRsb3R0IEjhu5MgQ2jDrSBNaW5o!5e0!3m2!1svi!2s!4v1565075588730!5m2!1svi!2s"
						width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>

				<!-- end map show is mobile -->
				<div class="mt-3 d-flex justify-content-between align-items-center">
					<div class="form-group">
						<select class="form-control" name="" id="">
							<option selected disabled value="">Tỉnh/ Thành phố</option>
							<option value="Thái Nguyên">Thái Nguyên</option>
							<option value="Hà Nội">Hà Nội</option>
						</select>
					</div>
					<div class="form-group">
						<select class="form-control" name="" id="">
							<option selected disabled value="">Quận/ Huyện</option>
							<option value="Quận 6">Quận 6</option>
							<option value="Quận 7">Quận 7</option>
						</select>
					</div>
				</div>
				<div class="result">
					<ul>
						<li>
							<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
							<p>123 Đường ABC, Phường X, Quận Z, HN </p>
						</li>
						<li>
							<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
							<p>123 Đường ABC, Phường X, Quận Z, HN </p>
						</li>
						<li>
							<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
							<p>123 Đường ABC, Phường X, Quận Z, HN </p>
						</li>
						<li>
							<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
							<p>123 Đường ABC, Phường X, Quận Z, HN </p>
						</li>
						<li>
							<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
							<p>123 Đường ABC, Phường X, Quận Z, HN </p>
						</li>
						<li>
							<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
							<p>123 Đường ABC, Phường X, Quận Z, HN </p>
						</li>
						<li>
							<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
							<p>123 Đường ABC, Phường X, Quận Z, HN </p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- show is not mobile -->
		<div class="col-md-8 mt-3  d-none d-md-block">
			<iframe
				src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d52731.18416405924!2d106.61408225443789!3d10.790719578445556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752eec7de29523%3A0x5a82b9c14c594ff4!2zWOG7lSBz4buRIFZpZXRsb3R0IEjhu5MgQ2jDrSBNaW5o!5e0!3m2!1svi!2s!4v1565075588730!5m2!1svi!2s"
				width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<!-- end show is not mobile -->

		<!-- <div class="col-12 px-4">
		<div class="row mt-3 shadow bg-white map_picker">

		
		</div>
		</div> -->
	</div>

</div>
<?php include('./include/footer.php')  ?>