$(document).ready(function () {
	$("#menu-toggle").click(function (e) {
		e.preventDefault();
		$("#wrapper").toggleClass("toggled");
	});
	const container = $('#page-content-wrapper');
	container.scroll(function () {

		if ($(this).scrollTop()) {
			$("#toTop").fadeIn();
		} else {
			$("#toTop").fadeOut();
		}
		if (container.scrollTop() + container.height() < container[0].scrollHeight - $("#footer").height()) {

			$('#toTop').css("position", "fixed");
			$('#toTop').css("bottom", "0");
		}
		if (container.scrollTop() + container.height() > container[0].scrollHeight - $("#footer").height()) {
			$('#toTop').css("position", "absolute");
			$('#toTop').css("bottom", $("#footer").height() + 41);
		}

	});


	$("#toTop").click(function () {
		container.animate({ scrollTop: 0 }, 1000);
	});


	// active menu news
	$('#menu_item a').click(function (e) {
		e.preventDefault();
		$(e.target.hash)[0].scrollIntoView({ behavior: "smooth", block: 'nearest', inline: "nearest" });
		$('#menu_item a.active').removeClass('active');
		$(this).addClass('active');
	});



	heightWindow = $(window).height();

	$('#page-content-wrapper').css("height", $(window).height());
	$('#sidebar-wrapper').css("height", $(window).height());

	if ($(".content_rule").offset()) {
		$(".content_rule").css("height", heightWindow - $(".content_rule").offset().top);
	}

	$(window).resize(function (e) {
		heightWindow = $(window).height();
		$('#page-content-wrapper').css("height", heightWindow);
		if ($(".content_rule").offset()) {
			$(".content_rule").css("height", heightWindow - $(".content_rule").offset().top);
		}
	})



	// $('#page-content-wrapper').scroll(function (e) {
	// 	;
	// 	if (($('#page-content-wrapper').scrollTop() + $('#page-content-wrapper').height()) === $('#page-content-wrapper')[0].scrollHeight) {
	// 		$('#page-content-wrapper').css("overflow", "hidden");
	// 	} else {
	// 		$('#page-content-wrapper').css("overflow", "auto");
	// 	}
	// });
});