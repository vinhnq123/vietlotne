<?php include('./include/header.php') ?>
<div class="container famous py-2">
    <h1 class="text-or title_primary fz-36 ml-2">Keno cùng người nổi tiếng</h1>
    <div class="row shadow">
        <div class="col-12">
            <div class="video_keno">
                <img src="./assets/images/peoples/hariwon.png" class="img-cover w-100" />
            </div>
        </div>
    </div>
    <div class="row mt-4 shadow py-2">
        <div class="col-md-6">
            <div class="video_keno">
                <img src="./assets/images/peoples/bb.png" class="img-cover w-100" />
            </div>
        </div>
        <div class="col-md-6 quote">
            <p>Chơi vừa vui, đơn giản lại có cơ hội đem 2 tỷ về nhà, ngại gì không thử chứ. Có khi BB trúng 2 tỷ thì
                mọi người sẽ không thấy BB trên các phương tiện thông tin đại chúng một thời gian :)))</p>
            <div class="author">BB Trần</div>
        </div>
    </div>
    <div class="row mt-4 shadow py-2">
        <div class="col-md-6 quote">
            <p>Hôm nay Hari biết đến cái sản phẩm “Vé số có kết quả nhanh nhất VN” 1 ngày Hari có thể có hơn 90 cơ hội
                để trúng thưởng đấy. Anh Xìn ơi, em mà trúng Keno em sẽ tăng lương cho anh nên anh Xìn cho em tiền để em
                chơi Keno nhé :)))</p>
            <div class="author">Hari Won</div>
        </div>
        <div class="col-md-6">
            <div class="video_keno">
                <img src="./assets/images/peoples/hariwon-2.png" class="img-cover w-100" />
            </div>
        </div>
    </div>
    <div class="row mt-4 shadow py-2">
        <div class="col-md-6">
            <div class="video_keno">
                <img src="./assets/images/peoples/nkh.png" class="img-cover w-100" />
            </div>
        </div>
        <div class="col-md-6 quote">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Adipiscing at in tellus integer feugiat scelerisque varius. Sit amet justo donec
                enim diam. Pulvinar elementum integer enim neque volutpat. Augue lacus viverra vitae congue eu .</p>
            <div class="author">Ngô Kiến Huy</div>
        </div>
    </div>
    <div class="row mt-4 shadow py-2">
        <div class="col-md-6 quote">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque elit eget gravida cum.</p>
            <div class="author">Diệu Nhi</div>
        </div>
        <div class="col-md-6">
            <div class="video_keno">
                <img src="./assets/images/peoples/dieu-nhi.png" class="img-cover w-100" />
            </div>
        </div>
    </div>
    <div class="w-100 text-center mt-5">
        <a href="#" class="btn btn-load-more px-5">Xem thêm</a>
    </div>
</div>

<?php include('./include/footer.php')  ?>