<?php include('./include/header.php') ?>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<img src="./assets/images/home/banner-1.png" alt="" class="img-fluid">
		</div>
		<div class="col-md-4">
			<img src="./assets/images/home/banner-2.png" alt="" class="img-fluid">
		</div>
		<div class="col-md-4">
			<img src="./assets/images/home/banner-3.png" alt="" class="img-fluid">
		</div>
	</div>


	<div class="px-3 my-4">
		<div class="row px-0 count_down align-items-center">
			<div class="col-md-6 txt_countdown">
				<p class="fw-600 mb-0 text-uppercase">Cùng chờ đón trong</p>
			</div>
			<div class="col-md-5 count_down_timer">
				<div class="flipper" data-reverse="true" data-datetime="2019-08-23 00:00:00" data-template="dd|HH|ii|ss"
					data-labels="Ngày|Giờ|Phút|Giây" id="myFlipper"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-8">
			<div class="home_left">
				<img src="./assets/images/home/choi-thu-trung-that.png" class="img-fluid" />
				<div class="action">
					<a class="btn btn-base btn-play" href="#">CHƠI NGAY</a>
				</div>
			</div>
		</div>
		<div class="col-xl-4">
			<div class="home_right">
				<img src="./assets/images/home/xo-so-keno.png" class="img-fluid" />
				<div class="action">
					<a class="btn btn-base btn-play" href="#">TÌM HIỂU NGAY</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-4">
		<div class="col-xl-8">
			<div class="home_left">
				<iframe class="home_left" style="height:472px" src="https://www.youtube.com/embed/Jr51eN3Z57M"
					frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
					allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-xl-4 slider_home home_right">
			<div id="carouselId" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselId" data-slide-to="0" class="active"></li>
					<li data-target="#carouselId" data-slide-to="1"></li>
					<li data-target="#carouselId" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner" role="listbox">
					<div class="carousel-item active">
						<img src="./assets/images/home/bb-tran.png" class="img-fluid" alt="First slide" />
						<div class="carousel-caption">
							<div class="tip_caption">
								Có khi BB trúng 2 tỷ thì mọi người sẽ không thấy BB trên các
								phương tiện thông tin đại chúng một thời gian :)))
							</div>
							<a class="link_more" href="#">KHÁM PHÁ THÊM >></a>
						</div>
					</div>
					<div class="carousel-item">
						<img src="./assets/images/home/bb-tran.png" class="img-fluid" alt="First slide" />
						<div class="carousel-caption">
							<div class="tip_caption">
								Lorem ipsum dolor sit amet consectetur adipisicing elit.
								Quibusdam pariatur cumque debitis quidem suscipit earum nihil a.
								Fugiat, molestiae ipsum.
							</div>
							<a class="link_more" href="#">KHÁM PHÁ THÊM >></a>
						</div>
					</div>
					<div class="carousel-item">
						<img src="./assets/images/home/bb-tran.png" class="img-fluid" alt="First slide" />
						<div class="carousel-caption">
							<div class="tip_caption">
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore
								odio tenetur iure natus eveniet quo rem aperiam adipisci sit
								optio?
							</div>
							<a class="link_more" href="#">KHÁM PHÁ THÊM >></a>
						</div>
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
	<div class="agency px-3">
		<div class="row my-4 text-center w-100">
			<p class="title_block fw-600 w-100 text-or">Keno - Chơi càng to càng vui</p>
		</div>
		<div class="row mt-3 shadow bg-white align-items-center mark">
			<div class="col-md-6 p-3">
				<h3>Bạn gặp vấn đề...</h3>
				<p>Muốn tìm một trò chơi mới cho buổi hoạt náo hàng tuần của công ty ?</p>
				<p> Tìm kiếm một hoạt động giải trí để thu hút KH cho cửa hàng của bạn ?</p>
				<p>Tìm kiếm một trò chơi cho sự kiện của bạn?</p>
				<a class="text-or fz-24 fw-600" href="#">
					<i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i> KENO sẽ giúp bạn giải quyết
				</a>
			</div>
			<div class="col-md-6">
				<img src="./assets/images/agency/question.png" alt="" class="img-fluid">
			</div>
		</div>
		<div class="row mt-3 shadow bg-white align-items-center px-0">
			<div class="col-md-6 px-0">
				<img src="./assets/images/agency/game.png" alt="" class="img-fluid">
			</div>
			<div class="col-md-6">
				<h3>Trải nghiệm game KENO</h3>
				<p> Mới lạ đầy thú vị</p>
				<p>Chơi thử nhận quà thật</p>
				<p>Quà nhỏ giải thưởng to lên đến 2 tỷ đồng</p>
				<p>Vừa tạo trò chơi giải trí, vừa có thể đem quà mang về</p>
				<div class="btn btn-fontCancel">TÌM HIỂU NGAY</div>
			</div>
		</div>
		<div class="row mt-3 shadow bg-white px-0 map_picker">
			<div class="col-md-4 px-0">
				<div class="title_map">Danh sách các điểm bán</div>
				<div class="px-3">

					<!-- //start map show is mobile -->
					<div class="map_mobile mt-3">
						<iframe
							src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d52731.18416405924!2d106.61408225443789!3d10.790719578445556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752eec7de29523%3A0x5a82b9c14c594ff4!2zWOG7lSBz4buRIFZpZXRsb3R0IEjhu5MgQ2jDrSBNaW5o!5e0!3m2!1svi!2s!4v1565075588730!5m2!1svi!2s"
							width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>

					<!-- end map show is mobile -->
					<div class="mt-3 d-flex justify-content-between align-items-center">
						<div class="form-group">
							<select class="form-control" name="" id="">
								<option selected disabled value="">Tỉnh/ Thành phố</option>
								<option value="Thái Nguyên">Thái Nguyên</option>
								<option value="Hà Nội">Hà Nội</option>
							</select>
						</div>
						<div class="form-group">
							<select class="form-control" name="" id="">
								<option selected disabled value="">Quận/ Huyện</option>
								<option value="Quận 6">Quận 6</option>
								<option value="Quận 7">Quận 7</option>
							</select>
						</div>
					</div>
					<div class="result">
						<ul>
							<li>
								<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
								<p>123 Đường ABC, Phường X, Quận Z, HN </p>
							</li>
							<li>
								<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
								<p>123 Đường ABC, Phường X, Quận Z, HN </p>
							</li>
							<li>
								<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
								<p>123 Đường ABC, Phường X, Quận Z, HN </p>
							</li>
							<li>
								<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
								<p>123 Đường ABC, Phường X, Quận Z, HN </p>
							</li>
							<li>
								<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
								<p>123 Đường ABC, Phường X, Quận Z, HN </p>
							</li>
							<li>
								<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
								<p>123 Đường ABC, Phường X, Quận Z, HN </p>
							</li>
							<li>
								<p class="fw-500"><i class="fas fa-map-marker-alt text-or"></i> Điểm bán 1</p>
								<p>123 Đường ABC, Phường X, Quận Z, HN </p>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- show is not mobile -->
			<div class="col-md-8 map_desktop">
				<iframe
					src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d52731.18416405924!2d106.61408225443789!3d10.790719578445556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752eec7de29523%3A0x5a82b9c14c594ff4!2zWOG7lSBz4buRIFZpZXRsb3R0IEjhu5MgQ2jDrSBNaW5o!5e0!3m2!1svi!2s!4v1565075588730!5m2!1svi!2s"
					width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<!-- end show is not mobile -->
		</div>


		<div class="row mt-4 px-0">
			<div class="col-12 px-0">
				<img src="./assets/images/home/banner_bottom.png" alt="" class="img-fluid d-none d-md-block" />
				<img src="./assets/images/home/banner_bottom_mobile.png" alt="" class="img-fluid d-block d-md-none" />
			</div>
		</div>
		<div class="row my-4 text-center w-100">
			<p class="title_block fw-600 w-100 text-or">Tìm điểm bán để trải nghiệm ngay</p>
		</div>
	</div>

</div>

<?php include('./include/footer.php')  ?>