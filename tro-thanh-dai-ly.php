<?php include('./include/header.php') ?>
<div class="agency">
    <div class="container py-2">

        <h1>Trở thành đại lý Vietlott</h1>
        <div class="row">
            <div class="col-12 mt-3 px-0">
                <img src="./assets/images/agency/banner_pc.png" alt="" class="img-fluid d-none d-md-block">
                <img src="./assets/images/agency/banner_mobile.png" alt="" class="img-fluid d-block d-md-none">
            </div>
        </div>
        <div class="row mt-3 shadow bg-white align-items-center mark">
            <div class="col-md-6">
                <h3>Bạn gặp vấn đề...</h3>
                <p>Muốn tìm một trò chơi mới cho buổi hoạt náo hàng tuần của công ty ?</p>
                <p> Tìm kiếm một hoạt động giải trí để thu hút KH cho cửa hàng của bạn ?</p>
                <p>Tìm kiếm một trò chơi cho sự kiện của bạn?</p>
                <a class="text-or fz-24 fw-600" href="#">
                    <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i> KENO sẽ giúp bạn giải quyết
                </a>
            </div>
            <div class="col-md-6">
                <img src="./assets/images/agency/question.png" alt="" class="img-fluid">
            </div>
        </div>
        <div class="row mt-3 shadow bg-white align-items-center">
            <div class="col-md-6 px-0">
                <img src="./assets/images/agency/game.png" alt="" class="img-fluid">
            </div>
            <div class="col-md-6">
                <h3>Trải nghiệm game KENO</h3>
                <p> Mới lạ đầy thú vị</p>
                <p>Chơi thử nhận quà thật</p>
                <p>Quà nhỏ giải thưởng to lên đến 2 tỷ đồng</p>
                <p>Vừa tạo trò chơi giải trí, vừa có thể đem quà mang về</p>
                <div class="btn btn-fontCancel">TÌM HIỂU NGAY</div>
            </div>
        </div>
        <div class="row mt-3 shadow bg-white py-3">
            <div class="col-md-3">
                <h4 class="fz-24 fw-600">Liên hệ ngay với chúng tôi</h4>
            </div>
            <div class="col-md-9">
                <p>Chúng tôi sẽ chuẩn bị quầy hoạt náo, lắp đặt các trang thiết bị để các bạn có thể trải nghiệm KENO
                    một
                    cách đơn giản, dễ dàng, tiết kiệm thời gian nhất.</p>
                <p>Đội ngũ nhân viên, PG của chúng tôi sẽ đến hỗ trợ để tạo không khí cho buổi hoạt náo tràn đầy niềm
                    cho.
                </p>
                <p> Tạo điều kiện để người dùng có thể dễ dàng hiểu rõ về trò chơi, cách để nhận quà từ Game KENO và cơ
                    hội
                    sở hưu giải thưởng thật lên đến 2 tỷ đồng.</p>
                <p>
                    <span class="fw-600 mr-5">Email: <span class="text-or">contact@vietlott.vn</span></span>
                    <span class="fw-600">
                        Hotline: <span class="text-or">0xx xxx xxxx</span>
                    </span>
                </p>
            </div>
        </div>
        <!-- step -->
        <div class="row text-center mt-3">
            <h2 class="fw-600 w-100">Gia tăng thu nhập cùng Vietlott</h2>

            <div class="d-flex step justify-content-between">
                <div class="box">
                    <div class="box_step">1</div>
                    <div class="txt_step">
                        <p>Đơn vị điền form thông tin để nhận tư vấn</p>
                    </div>
                </div>
                <div class="line_arrow">
                    <div class="position-relative w-100">
                        <div class="line"></div>
                        <div class="arrow">
                            <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box_step">2</div>
                    <div class="txt_step">
                        <p>Call Center của Vietlott liên hệ lại để tư vấn cho đơn vị</p>
                    </div>
                </div>
                <div class="line_arrow">
                    <div class="position-relative w-100">
                        <div class="line"></div>
                        <div class="arrow">
                            <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box_step">3</div>
                    <div class="txt_step">
                        <p>Đội Retail của Vietlott đến địa điểm đăng ký để khảo sát</p>
                    </div>
                </div>
                <div class="line_arrow">
                    <div class="position-relative w-100">
                        <div class="line"></div>
                        <div class="arrow">
                            <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box_step">4</div>
                    <div class="txt_step">
                        <p>Vietlott email thông báo khi đơn vị đủ điều kiện để tổ chức chương trình Vietlott</p>
                    </div>
                </div>
                <div class="line_arrow">
                    <div class="position-relative w-100">
                        <div class="line"></div>
                        <div class="arrow">
                            <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box_step">5</div>
                    <div class="txt_step">
                        <p>Hai bên bàn bạc
                            thỏa thuận và ký hợp đồng hợp tác</p>
                    </div>
                </div>
                <div class="line_arrow">
                    <div class="position-relative w-100">
                        <div class="line"></div>
                        <div class="arrow">
                            <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="box_step">6</div>
                    <div class="txt_step">
                        <p>Vietlott đến địa điểm lắp đặt máy cho đơn vị và hỗ trợ cho việc tổ chức chương trình</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- end step -->
        <!-- form -->
        <div class="row text-center mt-3 shadow bg-white">
            <div class="step_form w-100">
                <ul class="txt_step w-100">
                    <li>Bước 1</li>
                    <li class="active">Bước 2</li>
                    <li>Bước 3</li>
                    <li>Bước 4</li>
                    <li>Bước 5</li>
                    <li>Bước 6</li>
                </ul>
            </div>
            <div class="form w-100 mt-3">
                <!-- step 1 -->
                <h4 class="text-center fz-16">Vui lòng điền Thông tin đăng kí địa điểm kinh doanh Vietlott</h4>
                <form action="#" method="POST">
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="" id=""
                                    placeholder="Tên đơn vị/cá nhân đăng kí">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="" id="" placeholder="Địa chỉ đăng kí">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control" name="" id="">
                                    <option disabled selected>Quận/Huyện</option>
                                    <option>Phổ Yên</option>
                                    <option>2</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <select class="form-control" name="" id="">
                                        <option disabled selected>Tỉnh/Thành phố</option>
                                        <option>Hồ Chí Minh</option>
                                        <option>Thái Nguyên</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="" id="" placeholder="Email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="" id="" placeholder="Số điện thoại">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for=""></label>
                                <textarea class="form-control" name="" id="" rows="4" placeholder="Ghi chú"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-custom btn-send-form">Gửi</button>
                        </div>
                    </div>
                </form>
                <!-- end step 1 -->
                <!-- step 2 -->
                <!-- <div class="text-left">
                        <p class="fz-16 fw-500">
                            Call Center của Vietlott sẽ liên hệ lại hoặc liên hệ Hotline <span
                                class="text-or">0868XXXX</span> để được tư vấn ngay
                        </p>
                        <div class="txt mt-4">
                            <p> Nhân viên ở bộ phận call center của Vietlott sẽ liên hệ cho đơn vị/cá nhân đã gửi thông
                                tin
                                đăng
                                ký kinh doanh Vietlott để tư vấn kỹ càng về các thông tin liên quan đến việc kinh doanh
                                Vietlott</p>
                            <p> - Quyền lợi của đơn vị khi kinh doanh Vietlott</p>
                            <p>- Điều kiện để trở thành đại lý của Vietlott</p>
                            <p> - Thời gian từ lúc đăng ký thông tin đến lúc chính thức trở thành đại lý của Vietlott
                            </p>
                        </div>
                    </div> -->


                <!-- end step 2 -->
            </div>
        </div>

        <!-- end form -->
    </div>
</div>
<?php include('./include/footer.php')  ?>